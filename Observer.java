import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public abstract class Observer extends JFrame implements ActionListener{
	static JTextField text; 
	static JFrame welcome = new JFrame("Aardvark");  
	static JFrame mainMenu = new JFrame("Main Menu");
	static JFrame difficultyMenu = new JFrame("Difficulty Menu");
	static JButton button; 
	static JLabel label; 
	static protected Aardvark aardvark2;
	static int level;
	
	static username model = new username();
	static menuViewer view = new menuViewer();
	static menuControl control = new menuControl(model, view);
	
	protected Subject subject;
	
	public abstract void display();

	public void actionPerformed(ActionEvent e) 
	{ 
		String getCommand = e.getActionCommand(); 
		if (getCommand.equals("Start")) { 
			control.setPlayerName(text.getText());
			aardvark2.playerName = username.getName();
			welcome.dispose();
			mainMenu.setVisible(true); 
		} else if (getCommand.equals("Quit")){
			aardvark2.quitGame();
		} else if (getCommand.equals("Play")){
			difficultyMenu.setVisible(true);
		} else if (getCommand.equals("Rules")){
			aardvark2.rules();
		} else if (getCommand.equals("High Scores")){
			aardvark2.highScore();
		} else if (getCommand.equals("Simples")){
			level = 1;
			aardvark2.playGame();
		} else if (getCommand.equals("Not-so-simples")){
			level = 2;
			aardvark2.playGame();
		} else if (getCommand.equals("Super-duper-shuffled")){
			level = 3;
			aardvark2.playGame();
		} 
		
	}

}
