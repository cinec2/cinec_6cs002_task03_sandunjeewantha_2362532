
public class context {
	private strategyInterface strinterface;
	
	public context(strategyInterface strinterface) {
		this.strinterface = strinterface;
	}
	
	public void executeStr() {
		strinterface.autoplay();
	}
}
