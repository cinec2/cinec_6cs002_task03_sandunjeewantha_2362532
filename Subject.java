import java.util.ArrayList;
import java.util.List;

public class Subject{
	
	private List <Observer> observers = new ArrayList <Observer>();

	   public void attach(Observer observe){
	      observers.add(observe);		
	   }

	   public void notifyAllObservers(){
	      for (Observer observe : observers) {
	         observe.display();
	      }
	   } 	
	
}
