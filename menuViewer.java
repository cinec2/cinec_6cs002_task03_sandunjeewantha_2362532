
public class menuViewer {
	public void displayGUI() {
		Subject subject = new Subject();
		new welcomeMenu(subject);
		new mainMenuGUI(subject);
		new difficultyMenuGUI(subject);
		
		subject.notifyAllObservers();
	}
}
